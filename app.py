from flask import Flask, render_template
from flask.ext.frozen import Freezer
import sys
import markdown
import yaml
import os


FREEZER_BASE_URL = 'dunno'
FILE_EXTENTION = '.md'


def vars(filer):
    content = ''
    with open(filer, 'r') as fin:
        for line in fin:
            if not line.strip():
                break
            content += line
    return yaml.load( content )


def html(filer):
    with open(filer, 'r') as fin:
        content = fin.read().split('\n\n',1)[1].strip()
    return markdown.markdown(content)


app = Flask(__name__)
freezer = Freezer(app)


# The page get renderd here.
@app.route('/')
def page():
    intro = {'html': html('markdown/intro.md'), 'vars': vars('markdown/intro.md')}
    lesson = {'html': html('markdown/lesson.md')}

    data = {'intro':intro, 'lesson': lesson}
    return render_template('base.html', data=data)


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'build':
        freezer.freeze()
    else:
        app.run(host='0.0.0.0', port=8000, debug=True)
