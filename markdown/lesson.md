

lessen
------

###Trombone
De trombone is een koperen blaasinstrument en ziet er nog net zo uit als in vroegere tijden.De tenor-en bastrombone worden tegenwoordig het meest gebruikt.De toonhoogte verandert door de buis in en uit te schuiven. Ook de lipspanning be&iuml;nvloedt de toonhoogte.Wat kun je er mee gaan doen? De trombone wordt gebruikt in bijvoorbeeld: Harmonieorkest, Harmonieorkest, Fanfareorkest, Symphonieorkest, Jazzorkest, Bigband, Dansorkest, Koperensembles (trombonekwartet) en Carnavalsorkest.

###De tuba
Er zijn verschillende soorten tuba's De bariton, euphonium&#160;en de tenortuba&#160;zijn de kleinste van de tuba-familie. De grootste zijn de bassen. De verschillende soorten&#160; zijn Es-bas, Bes-bas,C-bas en F-bas. Ook de sousafoon behoort tot de bastuba-famlie. Een misverstand is dat je voor het bespelen van een tuba grote longen moet hebben. Het maakt niet uit hoeveel lucht je erin blaast, maar de manier waarop je dat doet. Je wordt geleerd met een goede buikademhaling te spelen. De tuba wordt veel gebruikt in samenspelverband, hoewel het instrument ook vaak gebruikt wordt als solo-instrument. Orkesten waar de tuba goed tot zijn recht komt zijn: harmonieorkest, fanfareorkest, drumfanfare, egerlanderorkest en carnavalsorkest.


###leerlingen-orkest
Om het samenspel te bevorderen start ik, bij voldoende aanmeldingen, een leerlingen-orkest. Personen die niet als leerling ingeschreven staan kunnen hier ook aan deelnemen. Ook begeleidt ik allerlei muziekgroepen in de regio Tilburg.

Als je ge&#239;nteresseerd bent in het leren bespelen van de tuba of de trombone of je wilt deeluitmaken van een leeringen-orkest, neem dan contact op met mij.
