img: trombone

Trombone
========

Wetenswaardigheden over het instrument.
--------------------------------------

De trombone is een koperen blaasinstrument en ziet er nog net zo uit als in vroegere tijden.De tenor-en bastrombone worden tegenwoordig het meest gebruikt.De toonhoogte verandert door de buis in en uit te schuiven. Ook de lipspanning be&iuml;nvloedt de toonhoogte.Wat kun je er mee gaan doen? De trombone wordt gebruikt in bijvoorbeeld:

* Harmonieorkest
* Fanfareorkest
* Symphonieorkest
* Jazzorkest
* Bigband
* Dansorkest
* Koperensembles (trombonekwartet)
* Carnavalsorkest

Als je ge&iuml;nterresseerd bent in het leren bespelen van de trombone neem dan contact op met
mij.
