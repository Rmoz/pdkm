Op deze pagina wordt u bladmuziek aan geboden voor:

* BrassbandsLooporkesten
* Carnavalsbands
* Kleine blazers-ensembles

De muziek is op maat gearrangeerd voor uw band. Zij staat dus garant voor een eigen bandgeluid en veel speelplezier. Op deze manier onderscheidt uw band zich van anderen. Behalve bladmuziek van de repertoirelijst, kunt u ook nieuwe composities, gelegenheidsmuziek of muziek in een nieuw jasje bestellen. Er bestaat ook de mogelijkheid om onder mijn leiding, middels &eacute;&eacute;n of twee repetitiebeurten (indien in de omgeving van Tilburg), de bestelde muziekstukken in te studeren.
Bij vragen kunt u contact opnemen met mij.
