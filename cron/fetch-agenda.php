<?php
header('Content-Type: application/json');
require_once('lib/CalFileParser.php');
include('lib/time_helper.php');


echo "Fetch iCal document and convert the document to json. \n";
echo "------- \n";
timer_start();


$FN_INPUT = 'https://www.google.com/calendar/ical/e3joks8gtikmadorm3j971eees%40group.calendar.google.com/public/basic.ics';
$FN_OUTPUT = '../public_html/data/agenda.json';
$AMOUNT = 2;


$cal = new CalFileParser();
$json = $cal->parse( $FN_INPUT, 'json' );
$decodedJson = array_reverse( json_decode( $json ), True );
$time = date(time());
$newjson = array();
$count = 0;


foreach($decodedJson as $item) {
    if (strtotime($item->dtend->date) > $time && $count < $AMOUNT) {
        array_push($newjson, $item);
        $count++;
    }
}


$file = fopen( $FN_OUTPUT, 'w' );
fwrite( $file, json_encode( $newjson ));
fclose( $file );


echo 'Done in '. timer_end(). ' seconds.';
