<?php

$LIB_DIR = '../phplib';

function cors() {
    if(isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Max-Age: 86400');
    }
    if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if(isset($_SERVER['HTTP_ACCES_CONTROL_REQUEST_METHOD']))
            header("Acces-Control-Allow-Methods: GET, POST, OPTIONS");
        if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Accces-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }
}


require '../phplib/Slim/Slim/Slim.php';
Slim\Slim::registerAutoloader();


$IMG_DIR = "../";
$PATH_IMG_DIR = '../../';


class Img{
    public $path;

    function __construct($path){
        $this->path = $path;
    }
}


function getFiles( $file_dir, $path ){
    $a = array();
    $files = scandir( $file_dir );
    foreach( $files as $file ){
       $i = new  Img( $path . $file );
       array_push( $a, $i );
    }
    return $a;
}


$app = new Slim\Slim(array('debug' => true));
$app->imgDir = $IMG_DIR;
$app->path = $PATH_IMG_DIR;


$app->get('/', function() use ( $app ) {
    echo json_encode( getFiles( $app->imgDir, $app->path ));
});


$app->post('/sendmail', function() use ( $app ) {
    cors();
    $response = $app->response;
    $response->headers->set('Content-Type', 'application/json');
    $request = $app->request;
    $json = $request->post();


    $response->setBody(json_encode(array('test' =>'done')));
});


$app->map('/:x+', function($x) {
})->via('OPTIONS');


$app->run();
