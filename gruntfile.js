'use strict';

module.exports = function(grunt){
  grunt.initConfig({
    pkg:grunt.file.readJSON('package.json'),
    compass: {
      compile: {
        files: {
          'static/css/master.css': 'sass/master.sass'
        }
      }
    },
    watch: {
      livereload: {
        options: {
          livereload: true
        },
    files: [
       'static/css/*.css',
       'static/js/*.js',
       'static/img/{,**/}*.{png,jpg,jpeg,gif,webp,svg}',
       'templates/*.jade'
    ]
      },
      compass: {
        files:'sass/*.sass',
        tasks:'compass'
      }
    }
  })
  grunt.loadNpmTasks('grunt-contrib-compass')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-livereload')
  grunt.loadNpmTasks('grunt-contrib-copy')

  grunt.registerTask('default',['compass', 'watch', 'livereload', 'flask'])
  grunt.registerTask('flask', 'Run flask server.', function() {
    var spawn = require('child_process').spawn;
    grunt.log.writeln('Starting Flask development server.');
    // stdio: 'inherit' let us see flask output in grunt
    var PIPE = {stdio: 'inherit'};
    spawn('python', ['app.py'], PIPE);
  });
}
