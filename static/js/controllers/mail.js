'use strict';

app.controller('MailCtrl', ['$scope', '$http',
   function ($scope, $http) {
        $scope.form = {};
        $scope.send_mail = function() {
            console.log($scope.form);
            $http.post('http://www.pdekokarrangementen.nl/api/sendmail', $scope.form)
            .success( function(response) {
                console.log('success '+response.test);
            })
            .error( function(response, status) {
                console.log('error '+ status);
            })
        }
    }
])
