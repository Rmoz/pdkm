'use strict';

app.controller('AgendaController', ['$scope', '$http',
    function($scope, $http) {
        $http.get('static/data/agenda.json')
        .success(function(response){
            console.log('ok ' + response);
            $scope.events = response
        })
        .error(function(response){
            console.log('boo ' +response);
        });
    }
]);

